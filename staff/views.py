from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from staff.models import Gadget


def list(request):
    gadgets = Gadget.objects.all()
    return render(request, 'staff/gadgets.html', context={'gadgets': gadgets})

def item(request, id):
    gadget = Gadget.objects.get(pk=id)
    return render(request, 'staff/gadget.html', context={'gadget': gadget})
