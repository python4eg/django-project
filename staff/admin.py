from django.contrib import admin

from staff.models import Gadget

# Register your models here.
admin.site.register(Gadget)