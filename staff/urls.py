
from django.urls import path
from . import views

app_name = 'staff'

# URLconf
urlpatterns = [
    path('', views.list, name='home'),
    path('<int:id>/', views.item, name='item')
]
