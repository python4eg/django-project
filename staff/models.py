from django.db import models


# Create your models here.
class Gadget(models.Model):
    name = models.IntegerField()
    description = models.TextField(blank=True)
    price = models.FloatField()
    available = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name}: {self.price}'